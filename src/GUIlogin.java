import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class GUIlogin implements ActionListener {

    JFrame f = new JFrame("OVapp");
    HashMap<String,String> logininfo = new HashMap<String,String>();

    JButton loginButton = new JButton("Login");
    JButton resetButton = new JButton("Reset");
    JTextField userIDfield = new JTextField();
    JPasswordField userPasswordField = new JPasswordField();
    JLabel useridLabel = new JLabel("userID:");
    JLabel userPasswordLabel = new JLabel("Password:");
    JLabel messageLabel = new JLabel();

    //feature1

    GUIlogin(HashMap<String,String> logininfoOriginal){

       logininfo = logininfoOriginal;
        useridLabel.setBounds(50,100,75,25);
        userPasswordLabel.setBounds(50,150,75,25);
        messageLabel.setBounds(125,250,250,35);
        messageLabel.setFont(new Font(null, Font.ITALIC,20));

        userIDfield.setBounds(125,100,200,25);
        userPasswordField.setBounds(125,150,200,25);

        loginButton.setBounds(125,200,100,25);
        loginButton.setFocusable(false);
        loginButton.addActionListener(this);

        resetButton.setBounds(225,200,100,25);
        resetButton.setFocusable(false);
        resetButton.addActionListener(this);

        f.add(loginButton);
        f.add(resetButton);
        f.add(userIDfield);
        f.add(userPasswordField);
        f.add(useridLabel);
        f.add(userPasswordLabel);
        f.add(messageLabel);
        f.setSize(420,420);
        f.setVisible(true);
//        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==resetButton) {
            userIDfield.setText("");
            userPasswordField.setText("");
        }

        if(e.getSource()==loginButton){

            String userID = userIDfield.getText();
            String password = String.valueOf(userPasswordField.getPassword());

            if(logininfo.containsKey(userID)) {
                if(logininfo.get(userID).equals(password)) {
                    messageLabel.setForeground(Color.green);
                    messageLabel.setText("Login succesfull");
                }
            }else {
                messageLabel.setForeground(Color.red);
                messageLabel.setText("Unknown userID/password");
            }
        }

    }
}
